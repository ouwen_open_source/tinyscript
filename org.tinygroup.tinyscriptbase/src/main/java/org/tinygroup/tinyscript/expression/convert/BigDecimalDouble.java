package org.tinygroup.tinyscript.expression.convert;

import java.math.BigDecimal;

import org.tinygroup.tinyscript.expression.Converter;

public class BigDecimalDouble implements Converter<BigDecimal,Double> {

	public Double convert(BigDecimal object) {
		return object.doubleValue();
	}

	public Class<?> getSourceType() {
		return BigDecimal.class;
	}

	public Class<?> getDestType() {
		return Double.class;
	}

}
