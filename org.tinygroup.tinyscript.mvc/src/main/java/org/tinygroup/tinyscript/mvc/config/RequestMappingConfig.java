package org.tinygroup.tinyscript.mvc.config;

import java.util.List;

import org.tinygroup.tinyscript.mvc.RequestMapping;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;


public class RequestMappingConfig implements RequestMapping{
	
	@XStreamAsAttribute
	private String name;
	
	@XStreamImplicit(itemFieldName="path")
	private List<String> paths;
	
	@XStreamImplicit(itemFieldName="method")
	private List<String> methods;
	
	@XStreamImplicit(itemFieldName="param")
	private List<String> params;
	
	@XStreamImplicit(itemFieldName="header")
	private List<String> headers;
	
	@XStreamImplicit(itemFieldName="consume")
	private List<String> consumes;
	
	@XStreamImplicit(itemFieldName="produce")
	private List<String> produces;

	public void setName(String name) {
		this.name = name;
	}

	public void setPaths(List<String> paths) {
		this.paths = paths;
	}

	public void setMethods(List<String> methods) {
		this.methods = methods;
	}

	public void setParams(List<String> params) {
		this.params = params;
	}

	public void setHeaders(List<String> headers) {
		this.headers = headers;
	}

	public void setConsumes(List<String> consumes) {
		this.consumes = consumes;
	}

	public void setProduces(List<String> produces) {
		this.produces = produces;
	}

	public String getName() {
		return name;
	}

	public String[] getPaths() {
		return convertArray(paths);
	}

	public String[] getMethods() {
		return convertArray(methods);
	}

	public String[] getParams() {
		return convertArray(params);
	}

	public String[] getHeaders() {
		return convertArray(headers);
	}

	public String[] getConsumes() {
		return convertArray(consumes);
	}

	public String[] getProduces() {
		return convertArray(produces);
	}
	
	private String[] convertArray(List<String> list){
		if(list!=null){
		   String[] ss = new String[list.size()];
		   return list.toArray(ss);
		}
		return null;
	}
}
