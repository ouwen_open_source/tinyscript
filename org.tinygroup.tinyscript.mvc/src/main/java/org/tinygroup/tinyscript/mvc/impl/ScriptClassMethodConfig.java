package org.tinygroup.tinyscript.mvc.impl;

import java.util.ArrayList;
import java.util.List;

import org.tinygroup.tinyscript.ScriptClassMethod;
import org.tinygroup.tinyscript.config.AbstractFunctionConfig;
import org.tinygroup.tinyscript.config.DefaultParameterConfig;
import org.tinygroup.tinyscript.config.ParameterConfig;

public class ScriptClassMethodConfig extends AbstractFunctionConfig{

	private List<ParameterConfig> parameters = new ArrayList<ParameterConfig>();
	
	public ScriptClassMethodConfig(ScriptClassMethod method) {
		super(method.getMethodName(), "ScriptClassMethod");
		if(method.getParamterNames()!=null){
		   for(String name:method.getParamterNames()){
			  parameters.add(new DefaultParameterConfig(name));
		   }
		}
		
	}
	
	public List<ParameterConfig> getParameters() {
		return parameters;
	}

	public String getDescription() {
		return null;
	}
	
}
