package org.tinygroup.tinyscript.mvc.context;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.tinygroup.context.impl.ContextImpl;
import org.tinygroup.tinyscript.mvc.ControllerContext;
import org.tinygroup.tinyscript.mvc.ScriptController;

@SuppressWarnings("serial")
public class DefaultControllerContext extends ContextImpl implements ControllerContext{

	private HttpServletRequest request;
	
	private HttpServletResponse response;
	
	private ScriptController controller;
	
	public DefaultControllerContext(HttpServletRequest request,HttpServletResponse response,ScriptController controller){
		this.request = request;
		this.response = response;
		this.controller = controller;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public ScriptController getScriptController() {
		return controller;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T get(String name){
		Object value = null;
		value = request.getParameter(name);
		if(value!=null){
		   return (T)value;
		}
		value = request.getHeader(name);
		if(value!=null){
		   return (T)value;
		}
		return super.get(name);
	}
	
	public boolean exist(String name){
		if(request.getParameter(name)!=null){
		   return true;
		}else if(request.getHeader(name)!=null){
			
		}
		return super.exist(name);
	}

}
