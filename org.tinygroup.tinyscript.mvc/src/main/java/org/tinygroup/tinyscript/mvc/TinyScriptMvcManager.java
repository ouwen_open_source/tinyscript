package org.tinygroup.tinyscript.mvc;

import java.util.List;

import org.tinygroup.tinyscript.ScriptSegment;
import org.tinygroup.tinyscript.mvc.config.ScriptControllerConfig;
import org.tinygroup.tinyscript.mvc.config.ScriptControllerConfigs;

/**
 * MVC配置管理器
 * @author yancheng11334
 *
 */
public interface TinyScriptMvcManager {

	/**
	 * 获取全部的控制层配置(组装后)
	 * @return
	 */
	List<ScriptController>  getScriptControllerList();
	
	/**
	 * 添加脚本类信息
	 * @param segment
	 */
	void addScriptClassConfig(ScriptSegment segment);
	
	/**
	 * 删除脚本类信息
	 * @param segment
	 */
	void removeScriptClassConfig(ScriptSegment segment);
	
	
	/**
	 * 批量添加脚本MVC配置
	 * @param configs
	 */
	void addScriptControllerConfigs(ScriptControllerConfigs configs);
	
	/**
	 * 添加单条脚本MVC配置
	 * @param configs
	 */
	void addScriptControllerConfig(ScriptControllerConfig config);
	
	/**
	 * 批量删除脚本MVC配置
	 * @param configs
	 */
	void removeScriptControllerConfigs(ScriptControllerConfigs configs);
	
	/**
	 * 删除单条脚本MVC配置
	 * @param config
	 */
	void removeScriptControllerConfig(ScriptControllerConfig config);
	
	/**
	 * 获取原始的全部控制层配置信息
	 * @return
	 */
	List<ScriptControllerConfig>  getScriptControllerConfigList();
	
	/**
	 * 获取某项控制层配置信息
	 * @param className
	 * @param functionName
	 * @return
	 */
	ScriptControllerConfig  getScriptControllerConfig(String className,String functionName);
}
