package org.tinygroup.tinyscript.mvc.config;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("controllers")
public class ScriptControllerConfigs {

	@XStreamImplicit
	private List<ScriptControllerConfig> controllers;

	public List<ScriptControllerConfig> getControllers() {
		return controllers;
	}

	public void setControllers(List<ScriptControllerConfig> controllers) {
		this.controllers = controllers;
	}
	
}
