package org.tinygroup.tinyscript;

import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.tinyrunner.Runner;
import org.tinygroup.tinyscript.mvc.TinyScriptMvcManager;
import org.tinygroup.tinyscript.mvc.config.ScriptControllerConfig;

public class TinyScriptMvcManagerTest{

	
	@BeforeClass
	public static void doBeforeClass() throws Exception {
		Runner.init("application.xml", null);
	}
	
	@Test
	public void testBase() throws Exception {
		TinyScriptMvcManager manager = BeanContainerFactory.getBeanContainer(TinyScriptMvcManagerTest.class.getClassLoader()).getBean("tinyScriptMvcManager");
		Assert.assertNotNull(manager);
		
		List<ScriptControllerConfig> configs = manager.getScriptControllerConfigList();
		Assert.assertEquals(6, configs.size());
		
		//一般URL配置
		ScriptControllerConfig config = null;
		config = manager.getScriptControllerConfig("HelloService", "helloByXml");
		Assert.assertNotNull(config);
		Assert.assertEquals("/hello", config.getUrl());
		Assert.assertArrayEquals(new String[]{"GET","POST"}, config.getRequestMapping().getMethods());
		
		//RESTful配置
	}
	
}
